extern crate deflate;
use deflate::deflate_bytes;

// https://bitbucket.org/jibsen/tinf/src/d4327ed5fe3826620e2c53c292d456d5cb6b5932/src/tinflate.c?at=default&fileviewer=file-view-default
// https://fat-crocodile.livejournal.com/194295.html
// https://habr.com/post/221849/

const MAXBITS: usize = 15;                      // maximum bits in a code
const MAXLCODES: usize = 286;                   // maximum number of literal/length codes
const MAXDCODES: usize = 30;                    // maximum number of distance codes
const MAXCODES: usize = MAXLCODES + MAXDCODES;  // maximum codes lengths to read
const FIXLCODES: usize = 288;                   // number of fixed literal/length codes

struct Huffman<'b> {
    count: &'b mut [usize],
    symbol: &'b mut [usize], 
}

impl <'b> Huffman <'b> {
    fn construct(&mut self, length: &[usize], length_count: usize) {
            /* count number of codes of each length */
        for i in 0..MAXBITS {
            self.count[i] = 0;
        }

        // count number of codes of each length
        for i in 0..length_count {
            self.count[length[i]] += 1;   // assumes lengths are within bounds
        }

        if self.count[0] == length_count {
            panic!("complete, but decode() will fail");
        }
        /*
        // check for an over-subscribed or incomplete set of lengths
        left = 1;                           // one possible code of zero length
        for (len = 1; len <= MAXBITS; len++) {
            left <<= 1;                     // one more bit, double codes left
            left -= h->count[len];          // deduct count from possible codes
            if (left < 0)
                return left;                // over-subscribed--return negative
        }       
        */
        let mut offs = [0usize; MAXBITS + 1];      // offsets in symbol table for each length

        for len in 1..MAXBITS {
            offs[len + 1] = offs[len] + self.count[len];
        }

        // put symbols in table sorted by length, by symbol order within each length
        for symbol in 0..length_count {
            if length[symbol] != 0 {
                self.symbol[offs[length[symbol]]] = symbol;
                offs[length[symbol]] += 1;
            }
        }
    }
}

struct State<'a> {
    output: &'a mut [u8],
    input: &'a [u8],
    bit_index: usize,
    outcnt: usize,
}

impl <'a> State <'a> {
    fn read_bits(&mut self, bit_count: usize) -> u8 {
        let mut res = 0;

        for i in 0..bit_count {
            res |= ((self.input[self.bit_index / 8] >> (self.bit_index % 8)) & 0x01) << i;
            self.bit_index += 1;
        }

        res
    }

    fn decode(&mut self, huffman: &Huffman) -> usize {
        let mut code: usize = 0; // len bits being decoded
        let mut first: usize = 0; // first code of length len
        let mut index: usize = 0; // index of first code of length len in symbol table

        for len in 1..MAXBITS { // current number of bits in code
            code |= self.read_bits(1) as usize;             // get next bit
            let count = huffman.count[len]; // number of codes of length len
            
            //println!("code {}, count {}", code, count);

            if (code as i32 - count as i32) < (first as i32) {
                // if length len, return symbol
                //println!("len {}", len);
                return huffman.symbol[index + code - first];
            }

            index += count; // else update for next length
            first += count;
            first <<= 1;
            code <<= 1;
        }

        panic!("ran out of codes");
    }

    fn codes(&mut self, lencode: &Huffman, distcode: &Huffman) {
        let lens: [usize; 29] = [ /* Size base for length codes 257..285 */
            3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
            35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258
        ];

        let lext: [usize; 29] = [ /* Extra bits for length codes 257..285 */
            0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2,
            3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0
        ];
    
        let dists: [usize; 30] = [ /* Offset base for distance codes 0..29 */
            1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193,
            257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145,
            8193, 12289, 16385, 24577
        ];

        let dext: [usize; 30] = [ /* Extra bits for distance codes 0..29 */
            0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
            7, 7, 8, 8, 9, 9, 10, 10, 11, 11,
            12, 12, 13, 13
        ];
        /*
        for index in 0..lencode.symbol.len() {
            println!("symbol[{}] = {}", index, lencode.symbol[index]);
            //println!("count[{}] = {}", index, lencode.count[index]);
        }
        */
        // decode literals and length/distance pairs
        loop {
            let mut symbol = self.decode(lencode);

            //if (symbol < 0)
            //    return symbol;              /* invalid symbol */
            
            if symbol < 256 {             
                // literal: symbol is the byte
                // write out the literal
                //if s->out != NIL {
                    //if s->outcnt == s->outlen {
                    //    return 1;
                    //}
                    //println!("char {}", symbol);
                    self.output[self.outcnt] = symbol as u8;
                //}
                self.outcnt += 1;
            } else if symbol > 256 {        
                // length
                // get and compute length
                symbol -= 257;
                
                if symbol >= 29 {
                    //return -10;             /* invalid fixed code */
                    panic!("invalid fixed code");
                }

                let len = lens[symbol] + self.read_bits(lext[symbol]) as usize;

                // get and check distance
                symbol = self.decode(distcode);

                //if (symbol < 0)
                //    return symbol;          /* invalid symbol */
                
                let dist = dists[symbol] + self.read_bits(dext[symbol]) as usize;

                //if (dist > s->outcnt)
                //    return -11;     // distance too far back

                // copy length bytes from distance bytes back
                //if (s->out != NIL) {
                    //if (s->outcnt + len > s->outlen)
                    //    return 1;
                    
                    //while (len--) {
                    for _ in 0..len {
                        //self.output[self.outcnt] = dist > self.outcnt ? 0 : self.output[self.outcnt - dist];
                        self.output[self.outcnt] = 0;
                        if dist < self.outcnt {
                            self.output[self.outcnt] = self.output[self.outcnt - dist];
                        }
                        self.outcnt += 1;
                    }
                //}
                //else
                //    s->outcnt += len;
            }

            if symbol == 256 {
                // end of block symbol 
                break;
            }
        }
    }

    fn dynamic(&mut self) {
        // get number of lengths in each table, check lengths
        let nlen = (self.read_bits(5) as usize) + 257;
        println!("nlen {}", nlen);
        let ndist = (self.read_bits(5) as usize) + 1;
        println!("ndist {}", ndist);
        let ncode = (self.read_bits(4) as usize) + 4;
        println!("ncode {}", ncode);

        if (nlen > MAXLCODES) || (ndist > MAXDCODES) {
            panic!("bad counts");
        }

        let mut lengths = [0usize; MAXCODES]; // descriptor code lengths
        let order: [usize; 19] = [ // permutation of code length codes
            16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
        ];

        for i in 0..ncode {
            lengths[order[i]] = self.read_bits(3) as usize;
        }

        let mut lencnt = [0usize; MAXBITS + 1];
        let mut lensym = [0usize; MAXLCODES];    // lencode memory
        let mut lencode = Huffman { count: &mut lencnt, symbol: &mut lensym}; 

        lencode.construct(&lengths, 19);

        // read length/literal and distance code length tables
        let mut index: usize = 0;

        while index < (nlen + ndist) {
            let mut symbol = self.decode(&lencode);

            //println!("decoded {}", symbol);

            if symbol < 16 {
                // length in 0..15
                lengths[index] = symbol;
                index += 1;
            } else {
                // repeat instruction
                let mut len: usize = 0;

                if symbol == 16 {         
                    // repeat last length 3..6 times
                    if index == 0 {
                        panic!("no last length!");
                    }
                    // last length
                    len = lengths[index - 1];
                    symbol = 3 + self.read_bits(2) as usize;
                } else if symbol == 17 {
                    // repeat zero 3..10 times
                    symbol = 3 + self.read_bits(3) as usize;
                } else {
                    // == 18, repeat zero 11..138 times
                    symbol = 11 + self.read_bits(7) as usize;
                }
                
                if (index + symbol) > (nlen + ndist) {
                    panic!("too many lengths!");
                }
                
                for _ in 0..symbol {
                    // repeat last or zero symbol times
                    lengths[index] = len;
                    index += 1;
                }
            }
        }
        /*
        for index in 0..lengths.len() {
            println!("lengths[{}] = {}", index, lengths[index]);
        }
        */
        // check for end-of-block code -- there better be one!
        if lengths[256] == 0 {
            panic!("missing end-of-block code");
        }

        // build huffman table for literal/length codes
        lencode.construct(&lengths, nlen);
        /*
        for index in 0..lencode.symbol.len() {
            println!("lencode.symbol[{}] = {}", index, lencode.symbol[index]);
        }
        */
        // build huffman table for distance codes
        let mut distcnt = [0usize; MAXBITS+1];
        let mut distsym = [0usize; MAXDCODES];
        let mut distcode = Huffman { count: &mut distcnt, symbol: &mut distsym };
        distcode.construct(&lengths[nlen..], ndist);

        // decode data until end-of-block code
        self.codes(&lencode, &distcode);
    }

    fn stored(&mut self) {
        //unsigned len;       /* length of stored block */

        /* discard leftover bits from current byte (assumes s->bitcnt < 8) */
        //s->bitbuf = 0;
        //s->bitcnt = 0;

        /* get length and check against its one's complement */
        //if s->incnt + 4 > s->inlen {
        //    panic!("not enough input");
        //}
        let mut input_byte_index: usize = 1;

        let mut len = self.input[input_byte_index] as u16;
        input_byte_index += 1;
        len |= (self.input[input_byte_index] as u16) << 8;
        input_byte_index += 1;

        //len = s->in[s->incnt++];
        //len |= s->in[s->incnt++] << 8;
        let mut nlen = self.input[input_byte_index] as u16;
        input_byte_index += 1;
        nlen |= (self.input[input_byte_index] as u16) << 8;
        input_byte_index += 1;    
        
        if len != (!nlen) {
            panic!("didn't match complement!");
        }
        /*
        if (s->in[s->incnt++] != (~len & 0xff) ||
            s->in[s->incnt++] != ((~len >> 8) & 0xff))
            return -2;                              /* didn't match complement! */
        */
        /* copy len bytes from in to out */
        //if (s->incnt + len > s->inlen)
        //    return 2;                               /* not enough input */

        //if (s->out != NIL) {
            //if (s->outcnt + len > s->outlen)
            //    return 1;                           /* not enough output space */

            for i in 0..(len as usize) {
                self.output[i] = self.input[input_byte_index + i];
            }
        //}
        //else {                                      /* just scanning */
        //    s->outcnt += len;
        //    s->incnt += len;
        //}

        /* done with a valid stored block */
        //return 0;
    }

    fn fixed(&mut self) {
        let mut lencnt = [0usize; MAXBITS+1];
        let mut lensym = [0usize; FIXLCODES];
        let mut distcnt = [0usize; MAXBITS+1];
        let mut distsym = [0usize; MAXDCODES];
        let mut lengths = [0usize; FIXLCODES];

        // construct lencode and distcode
        let mut lencode = Huffman {
            count: &mut lencnt,
            symbol: &mut lensym,
        };

        let mut distcode = Huffman {
            count: &mut distcnt,
            symbol: &mut distsym,
        };

        // literal/length table
        for symbol in 0..144 {
            lengths[symbol] = 8;
        }

        for symbol in 144..256 {
            lengths[symbol] = 9;
        }

        for symbol in 256..280 {
            lengths[symbol] = 7;
        }

        for symbol in 280..FIXLCODES {
            lengths[symbol] = 8;
        }

        lencode.construct(&lengths, FIXLCODES);

        // distance table
        for symbol in 0..MAXDCODES {
            lengths[symbol] = 5;
        }

        distcode.construct(&lengths, MAXDCODES);
        // decode data until end-of-block code
        self.codes(&lencode, &distcode);
    }

    fn show(&self) {
        for i in 0..self.outcnt {
            print!("{}", self.output[i] as char);
        }
    }
}

const EMB_DEFLATE_: usize = 15;

struct EmbDeflate {
    read_data: fn (data: &[u8]),
    output_buffer: &mut [u8],
}

fn emb_deflate_decode(dst: &mut [u8], src: &[u8])
{
    let mut state = State { output: dst, input: src, bit_index: 0, outcnt: 0 };
    loop {
        let last = state.read_bits(1);
        println!("last {}", last);
        let block_compression = state.read_bits(2);
        println!("block_compression {}", block_compression);

        match block_compression {
            0 => {
                println!("block: no compression");
                state.stored();
            },
            1 => {
                println!("block: fixed Huffman codes compression");
                state.fixed();
            },
            2 => {
                println!("block: dynamic Huffman codes compression");
                state.dynamic();
            },
            _ => {
                panic!("bad block compression");
            }
        }

        state.show();

        if last == 0 {
            break;
        }
        break;
    }
}

fn main() {
    let compressed:[u8; 101] = [
        0x15, 0x8d, 0x51, 0x0a, 0xc0, 0x20, 0x0c, 0x43, 0xff, 0x3d, 0x45, 0xae,
        0x56, 0x67, 0xdd, 0x8a, 0x5d, 0x0b, 0xd5, 0x21, 0xde, 0x7e, 0x0a, 0xf9,
        0x08, 0x21, 0x2f, 0xc9, 0x4a, 0x57, 0xcb, 0x12, 0x05, 0x5d, 0xec, 0xde,
        0x82, 0x18, 0xc6, 0xc3, 0x28, 0x4c, 0x05, 0x5e, 0x61, 0x72, 0x3f, 0x23,
        0x0d, 0x6a, 0x7c, 0xe2, 0xce, 0xc8, 0xe1, 0x8d, 0x0d, 0x73, 0x77, 0x3b,
        0xc8, 0x0a, 0x94, 0x29, 0x36, 0xe3, 0xa8, 0xba, 0x12, 0xa9, 0x62, 0xf9,
        0x17, 0x50, 0xa9, 0x9c, 0xb6, 0xc3, 0xe4, 0x60, 0xb8, 0xe9, 0xc2, 0x24,
        0x19, 0xe7, 0xa1, 0x7a, 0xec, 0x2d, 0xe9, 0x78, 0xfd, 0x65, 0x1b, 0x07,
        0xa5, 0x90, 0xce, 0xe9, 0x07,
    ];

    let mut dst: [u8; 500] = [0u8; 500];
    emb_deflate_decode(&mut dst, &compressed);
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dynamic_huffman() {
        let data = b"So far, deflate encoding with and without zlib and gzip metadata (zlib dictionaryies are not supported yet) has been is implemented. Speed-wise it's not quite up to miniz-levels yet (between 10% and twice as slow for most files, seems to be slow on very small files, close to miniz on larger ones).";
        let compressed = deflate_bytes(data);

        let mut dst: [u8; 500] = [0u8; 500];

        emb_deflate_decode(&mut dst, &compressed);
        
        for i in 0..data.len() {
            if data[i] != dst[i] {
                panic!("HELL");
            }
        }
    }

    #[test]
    fn static_huffman() {
        let data = b"Some shit";
        let compressed = deflate_bytes(data);

        let mut dst: [u8; 500] = [0u8; 500];

        emb_deflate_decode(&mut dst, &compressed);
        
        for i in 0..data.len() {
            if data[i] != dst[i] {
                panic!("HELL");
            }
        }
    }
}


